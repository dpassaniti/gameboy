#include "classes.h"

Slug::Slug(int objectIndex, int size, int shape, int tile, int xPosition, int yPosition)
	: Sprite(objectIndex, size, shape, tile, xPosition, yPosition)
	, xSpeed (1)
	, ySpeed (2)
	, xDirection(right)
	, jumping (false)
	, jumpHeight(64)
{}

void Slug::update()
{	
	//roll for chance to shoot and jump
	int chance = rand() % 100;
	if(chance == 0)
		shoot();
	if((chance == 50) && (!jumping))
		jump();
	
	//update position
	if(xDirection == right)
	{
		//check if it reached the end of the screen
		if(xPosition >= (240 - width))
			flip();
		else
			xPosition += xSpeed;
	}
	
	if(xDirection == left)
	{
		//check if it reached the end of the screen
		if(xPosition <= 0)
			flip();
		else
			xPosition -= xSpeed;
	}
	
	SetObjectX(objectIndex, xPosition);
	rollWheels();
	
	if(jumping)
	{
		if(yDirection == up)
		{
			yPosition -= ySpeed;
		}
		else
		{
			yPosition += ySpeed + 1;
		}
		
		if(yPosition <= jumpHeight)
		{
			yDirection = down;
		}
		
		//if returned to ground height
		if(yPosition >= (160 - height))
		{
			yPosition = 160 - height;
			jumping = false;
		}
			
		SetObjectY(objectIndex, yPosition);
	}
}

void Slug::flip()
{
	if(xDirection == right)
	{
		xDirection = left;
		//set ATTR1_HFLIP(binary mask: 0001 0000 0000 0000) 
		ObjBuffer[objectIndex].attr1 |= 0x1000;
	}
	else
	{
		xDirection = right;
		//unset ATTR1_HFLIP(binary mask: 1110 1111 1111 1111)
		ObjBuffer[objectIndex].attr1 &= 0xEFFF;
	}
}

void Slug::jump()
{
	jumping = true;
	yDirection = up;
}

void Slug::shoot()
{
	//determine the projectile's xPosition
	int projectilePosition;
	if(xDirection == right)
		projectilePosition = xPosition + 32;
	else
		projectilePosition = xPosition - 8;
	
	//find free place in OAM
	int i = 0;
	while(!(ObjBuffer[i].attr0 == ATTR0_HIDE))
		i++;

	//create Projectile object at found position
	Projectile projectile(i, projectilePosition, yPosition + 8, xDirection);
	
	//add it to the projectiles vector
	projectiles.push_back(projectile);
}

void Slug::rollWheels()
{
	//change tile to simulate animation
	if((xPosition % 2) == 0)
		ObjBuffer[objectIndex].attr2 = ATTR2_ID8(12);
	else
		ObjBuffer[objectIndex].attr2 = ATTR2_ID8(8);
}

void Slug::destroy()
{
	//destroy each of its projectiles
	for(std::vector<Projectile>::iterator p = projectiles.begin(); p != projectiles.end(); ++p)
		if((*p).visible)
			(*p).destroy();
	
	//destroy enemy itself
	Sprite::destroy();
}
	