
//{{BLOCK(spriteSheet)

//======================================================================
//
//	spriteSheet, 128x64@8, 
//	Transparent palette entry: 8.
//	+ palette 16 entries, not compressed
//	+ 128 tiles not compressed
//	Total size: 32 + 8192 = 8224
//
//	Time-stamp: 2014-05-17, 20:46:01
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.6
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_SPRITESHEET_H
#define GRIT_SPRITESHEET_H

#define spriteSheetTilesLen 8192
extern const unsigned short spriteSheetTiles[4096];

#define spriteSheetPalLen 32
extern const unsigned short spriteSheetPal[16];

#endif // GRIT_SPRITESHEET_H

//}}BLOCK(spriteSheet)
