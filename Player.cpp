#include "classes.h"

Player::Player(int objectIndex, int size, int shape, int tile, int xPosition, int yPosition)
	: Slug(objectIndex, size, shape, tile, xPosition, yPosition)
	, score(0)
	, shootingRatio(50)
	, shootCounter(shootingRatio)
{}

void Player::update()
{
	//check input
	
	//left button
	if(((REG_KEYINPUT & KEY_LEFT) == 0) && (xPosition > 0))
	{
		if(xDirection == right)//flip
			flip();
		xPosition -= xSpeed;
		SetObjectX(objectIndex, xPosition);
		rollWheels();
	}
	
	//right button
	if(((REG_KEYINPUT & KEY_RIGHT) == 0) && ((xPosition + width) < 240))
	{
		if(xDirection == left)//flip
			flip();
		xPosition += xSpeed;
		SetObjectX(objectIndex, xPosition);
		rollWheels();
	}
	
	//up button
	//jump only if it's not jumping already
	if(((REG_KEYINPUT & KEY_UP) == 0) && (!jumping))
	{
		jump();
	}
	
	//a button
	if(((REG_KEYINPUT & KEY_A) == 0) && (shootCounter == shootingRatio))
	{
		shootCounter = 0;
		shoot();
	}
	
	if(jumping)
	{
		if(yDirection == up)
		{
			yPosition -= ySpeed;
		}
		else
		{
			yPosition += ySpeed + 1;
		}
		
		if(yPosition <= jumpHeight)
		{
			yDirection = down;
		}
		
		//if returned to ground height
		if(yPosition >= (160 - height))
		{
			yPosition = 160 - height;
			jumping = false;
		}
			
		SetObjectY(objectIndex, yPosition);
	}
	
	//update shootCounter
	if(shootCounter < shootingRatio)
		shootCounter++;
}

void Player::rollWheels()
{
	//change tile to simulate animation
	if((xPosition % 2) == 0)
		ObjBuffer[objectIndex].attr2 = ATTR2_ID8(0);
	else
		ObjBuffer[objectIndex].attr2 = ATTR2_ID8(4);
}

void Player::onEnemyKilled()
{
	score++;
	//bonus speed every enemy killed
	if(shootingRatio > 5)
		shootingRatio--;
}