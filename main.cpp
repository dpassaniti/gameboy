#include "init.h"
#include "classes.h"

void wait(int);
void DrawNumber(int x, int y, int number);
void DrawText(int x, int y, const char text[]);
void spawnEnemy(std::vector<Slug>&);
void gameOver(int);

int main()
{	
	bool running = true;
	setOptions();
	loadData();
    
	//start screen
	SetPaletteBG(1, RGB(10, 17, 31));//change colour 1 temporarily to show text
	DrawText(0, 1, "---------Plastic Slug---------");
	DrawText(5, 10, "> press A to start <");
	//seed rand
	int seed = 0;
	while((REG_KEYINPUT & KEY_A) != 0)
	{
		seed++;
	}
	srand(seed);
	SetPaletteBG(1, RGB(0, 0, 0));
	
	initScreen();
	
	int bg2_offset = 0;//clouds
	int bg1_offset = 0;//buildings
	
	//create player
	Player player(0, 2, 0, 0, 0, 128);
	
	//create enemy vector
	std::vector<Slug> enemies;
	
	int spawnCounter = 0;
	int spawnRatio = 200;
	
	while(running)
	{	
		FlipBuffers();
		
		spawnCounter++;
		if(spawnCounter == spawnRatio)
		{
			spawnCounter = 0;
			spawnEnemy(enemies);
		}
		
		//parallax
		REG_BG2HOFS = bg2_offset;//clouds
		REG_BG1HOFS = bg1_offset;//buildings
		
		if((REG_KEYINPUT & KEY_LEFT) == 0)
		{
			bg2_offset--;//clouds
			bg1_offset -= 2;//buildings			
		}	
		if((REG_KEYINPUT & KEY_RIGHT) == 0)
		{			
			bg2_offset++;//clouds
			bg1_offset += 2;//buildings
		}
			
		//UPDATE and CHECK FOR COLLISIONS
		
		player.update();
		
		//score
		DrawNumber(26, 1, player.score);
		
		//update player's projectiles
		for(std::vector<Projectile>::iterator p = player.projectiles.begin(); p != player.projectiles.end(); ++p)
		{
			if((*p).visible)
			{
				(*p).update();
			
				//check collision with each enemy
				for(std::vector<Slug>::iterator s = enemies.begin(); s != enemies.end(); ++s)
				{
					if((*s).visible)
						if((*p).collidesWith(*s))
						{
							(*p).destroy();
							(*s).destroy();
							player.onEnemyKilled();
						}
				}
			}
		}
		
		//update enemies
		for(std::vector<Slug>::iterator s = enemies.begin(); s != enemies.end(); ++s)
		{
			if((*s).visible)
			{
				(*s).update();
				//update its projectiles
				for(std::vector<Projectile>::iterator p = (*s).projectiles.begin(); p != (*s).projectiles.end(); ++p)
				{
					if((*p).visible)
					{
						(*p).update();
					
						//check collision with player
						if((*p).collidesWith(player))
						{
							running = false;
						}
					}
				}
			}
		}
		
		UpdateObjects();
		WaitVSync();	
	};
	
	gameOver(player.score);

	return 0;
}

void wait(int milliseconds)
// pauses program
{
	for (int i = 0; i < (milliseconds / (1000 / 60)); i++)
	{
		WaitVSync();
	}
}

void DrawNumber(int x, int y, int number)
{
	int unit, tens, hundreds;
	unit = number % 10;
	tens = (number / 10) % 10;
	hundreds = (number / 100) % 10;
	
	SetTile(30, x, y, unit + 48);
	SetTile(30, x - 1, y, tens + 48);
	SetTile(30, x - 2, y, hundreds + 48);
}

void DrawText(int x, int y, const char text[])
{
	int k = 0;
	while(text[k] != '\0')
	{
		SetTile(30, x + k, y, (int)text[k]);
		k++;
	}
}

void spawnEnemy(std::vector<Slug>& enemies)
{
	//find free place in OAM
	int i = 0;
	while(!(ObjBuffer[i].attr0 == ATTR0_HIDE))
		i++;
	
	//create Slug object at found position
	Slug slug(i, 2, 0, 8, 202, 128);
	
	//add it to the enemies vector
	enemies.push_back(slug);
}

void gameOver(int score)
{
	DrawText(3, 1, "- Game over - Score: ");
	wait(5000);
}