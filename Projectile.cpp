#include "classes.h"

Projectile::Projectile(int objectIndex, int xPosition, int yPosition, direction xDirection)
	: Sprite(objectIndex, 0, 0, 64, xPosition, yPosition)
	, xDirection(xDirection)
	, xSpeed (2)
{}

void Projectile::update()
{
	//check it is on screen
	if ((xPosition > 0) && ((xPosition + width) < 240))
	{
		if(xDirection == right)
			xPosition += xSpeed;
		else
			xPosition -= xSpeed;
		
		SetObjectX(objectIndex, xPosition);
	}
	else
	{
		destroy();
	}	
}