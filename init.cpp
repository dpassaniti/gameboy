/*
define tiles
set display options
load charblocks/screenblocks
load sprite sheet
*/

#include "init.h"

//tiles
const uint8_t blank_tile[64] = {
	0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0,
};

const uint8_t sky_tile[64] = {
	3, 3, 3, 3, 3, 3, 3, 3, 
	3, 3, 3, 3, 3, 3, 3, 3,
	3, 3, 3, 3, 3, 3, 3, 3,
	3, 3, 3, 3, 3, 3, 3, 3,
	3, 3, 3, 3, 3, 3, 3, 3,
	3, 3, 3, 3, 3, 3, 3, 3,
	3, 3, 3, 3, 3, 3, 3, 3,
	3, 3, 3, 3, 3, 3, 3, 3,
};

const uint8_t cloud_tile[64] = {
	0, 0, 2, 3, 2, 3, 0, 3,
	3, 0, 0, 2, 3, 0, 3, 0,
	2, 0, 2, 3, 2, 3, 0, 3,
	0, 2, 3, 2, 3, 2, 3, 2,
	2, 3, 2, 3, 2, 3, 2, 0,
	3, 0, 3, 2, 3, 0, 0, 0,
	2, 0, 0, 3, 2, 3, 0, 3,
	0, 0, 3, 2, 3, 2, 0, 0,
};

const uint8_t sun_tile[64] = {
	3, 3, 4, 3, 3, 4, 3, 3,
	3, 3, 3, 4, 4, 3, 3, 3,
	4, 3, 4, 4, 4, 4, 3, 4,
	3, 4, 4, 4, 4, 4, 4, 3,
	3, 4, 4, 4, 4, 4, 4, 3,
	4, 3, 4, 4, 4, 4, 3, 4,
	3, 3, 3, 4, 4, 3, 3, 3,
	3, 3, 4, 3, 3, 4, 3, 3,
};

const uint8_t wall_tile1[64] = {
	1, 1, 1, 1, 1, 1, 1, 8,
	1, 5, 5, 5, 5, 5, 5, 8,
	1, 5, 5, 5, 5, 5, 5, 8,
	1, 5, 5, 5, 5, 5, 5, 8,
	1, 5, 5, 5, 5, 5, 5, 8,
	1, 5, 5, 5, 5, 5, 5, 8,
	1, 5, 5, 5, 5, 5, 5, 8,
	8, 8, 8, 8, 8, 8, 8, 8,
};

const uint8_t wall_tile2[64] = {
	1, 1, 1, 1, 1, 1, 1, 8,
	1, 6, 6, 6, 6, 6, 6, 8,
	1, 6, 6, 6, 6, 6, 6, 8,
	1, 6, 6, 6, 6, 6, 6, 8,
	1, 6, 6, 6, 6, 6, 6, 8,
	1, 6, 6, 6, 6, 6, 6, 8,
	1, 6, 6, 6, 6, 6, 6, 8,
	8, 8, 8, 8, 8, 8, 8, 8,
};

const uint8_t wall_tile3[64] = {
	1, 1, 1, 1, 1, 1, 1, 8,
	1, 7, 7, 7, 7, 7, 7, 8,
	1, 7, 7, 7, 7, 7, 7, 8,
	1, 7, 7, 7, 7, 7, 7, 8,
	1, 7, 7, 7, 7, 7, 7, 8,
	1, 7, 7, 7, 7, 7, 7, 8,
	1, 7, 7, 7, 7, 7, 7, 8,
	8, 8, 8, 8, 8, 8, 8, 8,
};

void setOptions()
{
	// Set display options.
	REG_DISPCNT = DCNT_MODE0 | DCNT_OBJ | DCNT_BG0 | DCNT_BG1 | DCNT_BG2 | DCNT_BG3;

	// Set background options
	REG_BG3CNT = BG_CBB(1) | BG_SBB(27) | BG_8BPP | BG_REG_32x32;//sky
	REG_BG2CNT = BG_CBB(1) | BG_SBB(28) | BG_8BPP | BG_REG_32x32;//clouds
	REG_BG1CNT = BG_CBB(1) | BG_SBB(29) | BG_8BPP | BG_REG_32x32;//buildings
	REG_BG0CNT = BG_CBB(0) | BG_SBB(30) | BG_8BPP | BG_REG_32x32;//text

	REG_BG0HOFS = 0;
	REG_BG0VOFS = 0;
	REG_BG1HOFS = 0;
	REG_BG1VOFS = 0;
	REG_BG2HOFS = 0;
	REG_BG2VOFS = 0;
	REG_BG3HOFS = 0;
	REG_BG3VOFS = 0;
}

void loadData()
{
	// Set up bg palette.
	SetPaletteBG(0, RGB(0, 0, 0));//transparent
	SetPaletteBG(1, RGB(0, 0, 0));// black
	SetPaletteBG(2, RGB(31, 31, 31));// white
	SetPaletteBG(3, RGB(12, 25, 31));// light-blue
	SetPaletteBG(4, RGB(31, 31, 0));// yellow
	SetPaletteBG(5, RGB(27, 9, 9)); //red
	SetPaletteBG(6, RGB(9, 9, 17)); // dark blue		
	SetPaletteBG(7, RGB(31, 24, 18)); // light orange
	SetPaletteBG(8, RGB(25, 25, 25)); // grey

	//load font tiles in charblock 0
	for (int i = 0; i < 128; i++)
	{
		LoadTile8(0, i, font_medium[i]);
	}

	//load charblock 1 tiles
	LoadTile8(1, 0, blank_tile);
	LoadTile8(1, 1, sky_tile);
	LoadTile8(1, 2, cloud_tile);
	LoadTile8(1, 3, sun_tile);
	LoadTile8(1, 4, wall_tile1);
	LoadTile8(1, 5, wall_tile2);
	LoadTile8(1, 6, wall_tile3);
	
	//load sprite sheet
	LoadPaletteObjData(0, spriteSheetPal, sizeof(spriteSheetPal));
	LoadTileData(4, 0, spriteSheetTiles, sizeof(spriteSheetTiles));
	ClearObjects();
}

void initScreen()
{
	//clear screenblock 27 to 30

	for(int x = 0; x < 32; x++)
	{
		for(int y = 0; y < 32; y++)
			{
				SetTile(27, x, y, 0);
			}
	}

	for(int x = 0; x < 32; x++)
	{
		for(int y = 0; y < 32; y++)
			{
				SetTile(28, x, y, 0);
			}
	}

	for(int x = 0; x < 32; x++)
	{
		for(int y = 0; y < 32; y++)
			{
				SetTile(29, x, y, 0);
			}
	}

	for (int x = 0; x < 32; ++x)
	{
		for (int y = 0; y < 32; ++y)
		{
			SetTile(30, x, y, 0);
		}
	}

	//fill screenblock 27 with sky tiles
	for (int x = 0; x < 32; ++x)
	{
		for (int y = 0; y < 32; ++y)
		{
			SetTile(27, x, y, 1);
		}
	}

	//put a sun in screenblock 27
	SetTile(27, 16, 3, 3);

	//put some clouds in screenblock 28
	SetTile(28, 5, 3, 2);
	SetTile(28, 11, 2, 2);
	SetTile(28, 17, 1, 2);
	SetTile(28, 26, 5, 2);
	SetTile(28, 28, 4, 2);
	SetTile(28, 31, 2, 2);

	//put some buildings in screenblock 29
	//building one
	for(int x = 0; x < 5; x++)
	{
		for(int y = 19; y > 10; y--)
			SetTile(29, x, y, 4);
	};
	//building two
	for(int x = 5; x < 9; x++)
	{
		for(int y = 19; y > 7; y--)
			SetTile(29, x, y, 5);
	};
	//building three
	for(int x = 9; x < 17; x++)
	{
		for(int y = 19; y > 11; y--)
			SetTile(29, x, y, 6);
	};
	//building four
	for(int x = 17; x < 21; x++)
	{
		for(int y = 19; y > 12; y--)
			SetTile(29, x, y, 5);
	};
	//building five
	for(int x = 21; x < 23; x++)
	{
		for(int y = 19; y > 8; y--)
			SetTile(29, x, y, 4);
	};
	//building six
	for(int x = 23; x < 30; x++)
	{
		for(int y = 19; y > 10; y--)
			SetTile(29, x, y, 6);
	};
	//building seven
	for(int x = 30; x < 32; x++)
	{
		for(int y = 19; y > 6; y--)
			SetTile(29, x, y, 4);
	};
}

