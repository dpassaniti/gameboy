/*
define tiles
set display options
load charblocks/screenblocks
load sprite sheet
*/

#ifndef INIT_H
#define INIT_H

#include <stdint.h>
#include "gba.h"
#include "font.h"
#include "spriteSheet.h"

extern const uint8_t blank_tile[64];
extern const uint8_t sky_tile[64];
extern const uint8_t cloud_tile[64];
extern const uint8_t sun_tile[64];
extern const uint8_t wall_tile1[64];
extern const uint8_t wall_tile2[64];
extern const uint8_t wall_tile3[64];

void setOptions();
void loadData();
void initScreen();

#endif