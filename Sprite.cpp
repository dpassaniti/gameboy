#include "classes.h"

Sprite::Sprite(int objectIndex, int size, int shape, int tile, int xPosition, int yPosition)
	: objectIndex(objectIndex)
	, size(size)
	, shape(shape)
	, tile(tile)
	, xPosition(xPosition)
	, yPosition(yPosition)
	, visible(true)
{
	//find width and height in pixels from size/shape
	switch(size)
	{
		case 0:
			if (shape == 0)
				{
					width = 8;
					height = 8;
				}
			else if (shape == 1)
				{
					width = 16;
					height = 8;
				}
			else if (shape == 2)
				{
					width = 8;
					height = 16;
				}
			break;
			
		case 1:
			if (shape == 0)
				{
					width = 16;
					height = 16;
				}
			else if (shape == 1)
				{
					width = 32;
					height = 8;
				}
			else if (shape == 2)
				{
					width = 8;
					height = 32;
				}
			break;
			
		case 2:
			if (shape == 0)
				{
					width = 32;
					height = 32;
				}
			else if (shape == 1)
				{
					width = 32;
					height = 16;
				}
			else if (shape == 2)
				{
					width = 16;
					height = 32;
				}
			break;
			
		case 3:
			if (shape == 0)
				{
					width = 64;
					height = 64;
				}
			else if (shape == 1)
				{
					width = 64;
					height = 32;
				}
			else if (shape == 2)
				{
					width = 32;
					height = 64;
				}
			break;
			
		default:
			break;
	}
	
	//set object
	SetObject(objectIndex,
	          ATTR0_SHAPE(shape) | ATTR0_8BPP | ATTR0_REG | ATTR0_Y(yPosition),
			  ATTR1_SIZE(size) | ATTR1_X(xPosition),
			  ATTR2_ID8(tile));
}

bool Sprite::collidesWith(Sprite object)
//check collision between 2 objects
{
	bool yInRange = false;
	bool xInRange = false;
	
	//upper
	if((yPosition <= (object.yPosition + object.height)) && (yPosition >= (object.yPosition)))
		yInRange = true;
	//lower
	else if(((yPosition + height) <= (object.yPosition + object.height)) && ((yPosition + height) >= (object.yPosition)))
		yInRange = true;
		
	//left
	if((xPosition <= (object.xPosition + object.width)) && (xPosition >= (object.xPosition)))
		xInRange = true;
		
	//right
	else if(((xPosition + width) <= (object.xPosition + object.width)) && ((xPosition + width) >= (object.xPosition)))
		xInRange = true;
		
	if(xInRange && yInRange)
		return true;
		
	return false;
}

void Sprite::destroy()
{
	visible = false;
	SetObject(objectIndex, ATTR0_HIDE, 0, 0);
}