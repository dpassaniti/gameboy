/*
CLASS PROTOTYPES
*/

#ifndef CLASSES_H
#define CLASSES_H

#include <stdint.h>
#include <stdlib.h>
#include <vector>
#include "gba.h"

enum direction {left, right, up, down};

//represent an object
class Sprite
{
	public:
		Sprite(int objectIndex, int size, int shape, int tile, int xPosition, int yPosition);
		
		int objectIndex;//memory index of represented object
		int size;
		int shape;
		int tile;
		int xPosition;
		int yPosition;
		int width;
		int height;
		bool visible;//false when object is destroyed
		
		bool collidesWith(Sprite);
		void destroy();
};

//projectile object
class Projectile : public Sprite
{
	public:
		Projectile(int objectIndex, int xPosition, int yPosition, direction xDirection);
		
		direction xDirection;
		int xSpeed;
		
		void update();
};

//enemy
class Slug : public Sprite
{
	public:
		Slug(int objectIndex, int size, int shape, int tile, int xPosition, int yPosition);
	
		int xSpeed;
		int ySpeed;
		direction xDirection;
		direction yDirection;
		bool jumping;
		int jumpHeight;
		std::vector<Projectile> projectiles;
		
		void update();
		void jump();
		void shoot();
		void flip();
		void rollWheels();
		void destroy();
};

//player
class Player : public Slug
{
	public:
		Player(int objectIndex, int size, int shape, int tile, int xPosition, int yPosition);
	
		int score;
		int shootingRatio;
		int shootCounter;//used to check if player can shoot
		
		void rollWheels();
		void update();
		void onEnemyKilled();//increment score, decrement shooting ratio (if adequate)
};

#endif